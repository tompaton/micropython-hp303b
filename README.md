# HP303b barometric pressure sensor

Basic MicroPython module to read temperature and pressure from
HP303b sensor on D1 mini Barometric Pressure Shield.

https://docs.wemos.cc/en/latest/d1_mini_shiled/barometric_pressure.html

Data sheet:
https://docs.wemos.cc/en/latest/_static/files/hp303b_datasheet.pdf


## Example

    from machine import I2C, Pin
    from time import sleep
    from hp303b import HP303B

    bus = I2C(scl=Pin(5), sda=Pin(4), freq=100000)
    bus.start()

    barometer = HP303B(bus)
    barometer.setup()

    while True:
        print(barometer.read())
        sleep(1)


## To-do

* Add support for other measurement and oversampling rates

  Currently only a basic measurement and oversampling rate is supported,
  suitable for a small weather station.

* Add unit tests

* Is there a better way to unpack data than the read_reg method?
