from machine import I2C, Pin
from time import sleep
from hp303b import HP303B

bus = I2C(scl=Pin(5), sda=Pin(4), freq=100000)
bus.start()

barometer = HP303B(bus)
barometer.setup()

while True:
    print(barometer.read())
    sleep(1)
